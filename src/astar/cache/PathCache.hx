package astar.cache;

import astar.types.PathNode;


class PathCache
{
    public var size(get, never): Int;
    public var hits(default, null): Int;
    public var misses(default, null): Int;

    var cache: BubbleList<CachedPath>;
    var cacheHashMap: Map<Int, CachedPath>;

    var cachedPathPool: CachedPathPool;
    var cachedPathNodePool: CachedPathNodePool;

    public function new(size: Int = 100)
    {
        hits = 0;
        misses = 0;

        cache = new BubbleList<CachedPath>(size);
        cacheHashMap = new Map<Int, CachedPath>();

        cachedPathPool = new CachedPathPool();
        cachedPathNodePool = new CachedPathNodePool();
    }

    /**
        Checks the cache for an existing path solution to the given start and end coordinates.

        If a path is found, it is appended to the end of the given `SearchResult`.

        @param start The state identifier of the starting node.
        @param end The state identifier of the ending node.
        @param result The search result object, into which the cached path will be loaded if a solution is found.
        @return Value indicating whether a solution was found in the cache.
    **/
    public function solve(start: Int, end: Int, result: SearchResult, costFromStart: Float, ?maxCost: Float): CacheResult
    {
        var hash: Int = hash(start, end);

        var cachedPath: CachedPath = get(hash);

        if (cachedPath == null)
        {
            // No path is cached for the given start-end combination.
            misses++;
            return Miss;
        }

        if (!cachedPath.matches(start, end))
        {
            // The given start-end combination is a hash collision.
            misses++;
            return Miss;
        }

        // Cache hit!
        hits++;

        cache.bubble(cachedPath);

        if (!cachedPath.hasSolution())
        {
            // It is cached that there is no solution to the start-end combination.
            return NoSolution;
        }

        // We have a solution for the requested search.
        var totalCost: Float = costFromStart + cachedPath.startNode.costToEnd;
        if (maxCost != null && totalCost > maxCost)
        {
            // ... but it exceeds the max cost
            return MaxCostExceeded;
        }

        // Append it to the given search result.
        result.appendCachedNodes(cachedPath.startNode);

        return Hit;
    }

    /**
        Adds a solved path to the cache.

        @param end The final node in the path, as generated by `Graph.findPath()`.
    **/
    public function addSolution(end: PathNode)
    {
        var endNode: CachedPathNode = cachedPathNodePool.getCachedPathNode(end.state, end.transition, null, 0);

        // Start iterating backwards from the end node.
        var iter: PathNode = end;
        var next: CachedPathNode = endNode;

        while (iter.previous != null)
        {
            // iter is still pointing to the next node in the path.
            // The total cost to the end is the cost to move to the next node, plus the next node's cost to the end.
            var costToEnd: Float = iter.getCostFromPrevious() + next.costToEnd;

            // Move the iterator to point to this node.
            iter = iter.previous;

            // Create the node and simultaneously store it for the next loop.
            var node: CachedPathNode = cachedPathNodePool.getCachedPathNode(iter.state, iter.transition, next, costToEnd);

            // Store a path that goes from this node to the end.
            var hash: Int = hash(node.state, endNode.state);

            var path: CachedPath = cachedPathPool.getSolvedCachedPath(node, endNode, hash);

            addPath(path);

            next = node;
        }
    }

    /**
        Adds a pair of start and end coordinates to the cache, which are known to have no solution.

        @param start The state identifier of the starting node.
        @param end The state identifier of the ending node.
        @param endX The x-coordinate of the ending point.
        @param endY The y-coordinate of the ending point.
    **/
    public function addNoSolution(start: Int, end: Int)
    {
        var hash: Int = hash(start, end);

        var path: CachedPath = cachedPathPool.getNoSolutionCachedPath(start, end, hash);

        addPath(path);
    }

    /**
        Empties the cache.

        It is recommended to keep and reuse the `PathCache` object, since
        internal objects are being pooled for better performance.
    **/
    public function reset(freeMemory: Bool = false)
    {
        while (cache.length > 0)
        {
            var path: CachedPath = cache.last();

            if (path.hasSolution())
            {
                if (path.startNode.next.next == null)
                {
                    // If the node after this one's starting node has no next node,
                    // then it is the ending node.
                    // In this case we should free it here, since no other path will have it as its starting node.
                    cachedPathNodePool.put(path.startNode.next);
                }

                // Put its starting node back to the pool.
                // We only know for sure that the starting node is only referenced by this path,
                // all the rest in the path can be referenced by multiple, so we won't return them to the pool here.
                cachedPathNodePool.put(path.startNode);
            }

            freePath(path.hash);
        }

        if (freeMemory)
        {
            cachedPathPool.clear();
            cachedPathNodePool.clear();
        }
    }

    /**
        Updates the configuration of the cache.

        Should only be called after `reset()`.

        @param size The new size of the cache.
    **/
    public inline function configure(size: Int)
    {
        cache = new BubbleList<CachedPath>(size);
    }

    public inline function get_size(): Int
    {
        return cache.capacity;
    }

    function addPath(path: CachedPath)
    {
        // Check if a path already exists with this hash
        if (get(path.hash) != null)
        {
            freePath(path.hash);
        }

        var popped: CachedPath = cache.insert(path);

        if (popped != null)
        {
            // Inserting the given path caused the list to exceed capacity.
            // Thus the last path in order was popped and should be deleted.
            freePath(popped.hash);
        }

        cacheHashMap[ path.hash ] = path;
    }

    inline function freePath(hash: Int)
    {
        var path: CachedPath = get(hash);

        // Remove from the list and the map.
        cache.remove(path);
        cacheHashMap.remove(hash);

        // Free up memory.
        cachedPathPool.put(path);
        path.startNode = null;
    }

    inline function get(hash: Int): CachedPath
    {
        return cacheHashMap[ hash ];
    }

    public static inline function hash(start: Int, end: Int): Int
    {
        return Std.int((start + end) * (start + end + 1) / 2 + end);
    }
}
