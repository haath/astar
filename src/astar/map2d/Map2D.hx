package astar.map2d;

import astar.map2d.Direction;
import astar.map2d.heuristics.*;
import astar.map2d.types.*;
import astar.types.NextNodeCallback;


class Map2D extends Graph
{
    public var width(default, null): Int;
    public var height(default, null): Int;
    public var movementDirection(default, null): MovementDirection;

    var map: Array<Int>;
    var costs: Map<Int, Map<Direction, Float>>;
    var heuristicFunction: HeuristicFunction;
    var heuristicWeight: Float;

    public function new(width: Int, height: Int, movementDirection: MovementDirection)
    {
        super();

        if (width <= 0 || width > 0xFFFF || height <= 0 || height > 0xFFFF)
        {
            throw 'dimensions for 2D maps must be between 1 and 65535';
        }

        this.width = width;
        this.height = height;
        this.movementDirection = movementDirection;

        map = null;
        costs = null;

        heuristicFunction = null;
        heuristicWeight = 1.0;
    }

    /**
     * Loads the world grid into the graph.
     *
     * The array should have `width`x`height` elements, which contain the grid packed
     * row by row. So the first element is the cell 0x0, the second element is the cell 1x0 and so on.
     *
     * The values in the cells of the grid are arbitrary, and shall be chosen by the user to represent the various
     * *types* of tiles present in the world, which are differentiated by different passability and/or traversal costs.
     *
     * The values of passable cells should then be used as keys for defining the costs that are passed to the `setCosts()` method.
     *
     * **Note:** Invoking this method resets the path cache.
     *
     * @param world An array with `width`x`height` elements containing the grid tile values.
     */
    public function setMap(map: Array<Int>)
    {
        if (map.length != width * height)
        {
            throw 'Invalid map size, expected: $width x $height = ${width * height}';
        }

        resetCache();

        this.map = map;
    }

    /**
     * Overrides the default cost function, for calculating the heuristic distance
     * that is part of the A* algorithm.
     *
     * The default function is chosen according to the movement directions.
     *
     * - `FourWay`: Manhattan Distance
     * - `EightWay`: Diagonal Distamce
     *
     * For 8-way movement you may also consider the Euclidean distance.
     *
     * ```haxe
     * setHeuristic(new EuclideanDistance());
     * ```
     *
     *  Can also set the weight with which the algorithm will consider the heuristic distance to the destination.
     *  The default weight is `1.0`.
     *  This can be used as a tie-breaking method by setting it to a value slightly larger than `1.0`.
     *
     *  ```
     *  weight = 1.0 + (minimum cost of taking one step)/(expected maximum path length)
     *  ```
     *
     * **Note:** Invoking this method resets the path cache.
     *
     * @param heuristic the heuristic function
     * @param weight the weight given to the heuristic
     */
    public function setHeuristic(heuristic: HeuristicFunction, weight: Float = 1.0)
    {
        resetCache();

        heuristicFunction = heuristic;
        heuristicWeight = weight;
    }

    /**
     * Sets the traversal costs for various cell values in the world.
     *
     * The `costs` map should have a key for each type of passable cell in the grid, which then specifies another map
     * which contains the cost of movement **into** the respective cell in each direction.
     *
     * An example with 4-way movement, where only cells with value `4` are passable:
     *
     * ```haxe
     * costs = [
     *     4 => [ N => 1, S => 1, W => 1, E => 1]
     * ];
     * ```
     *
     * Another example where cells with value `3` are *slopes*, where it's easier to move in from the south than the north.
     *
     * ```haxe
     * costs = [
     *     3 => [ N => 1.5, S => 0.5, W => 1, E => 1]
     * ];
     * ```
     *
     * Only cells whose value is present in the `costs` map will be considered passable, all the rest will be treated as obstacles.
     *
     * **Note:** Invoking this method resets the path cache.
     *
     * @param costs The map of costs, as described above.
     */
    public function setCosts(costs: Map<Int, Map<Direction, Float>>)
    {
        resetCache();

        this.costs = costs;

        if (heuristicFunction == null)
        {
            heuristicFunction = switch movementDirection
                {
                    case FourWay:
                        new ManhattanDistance();

                    case EightWay | EightWayObstructed | EightWayHalfObstructed:
                        new DiagonalDistance(getMinCost(N | W | E | S), getMinCost(NE | NW | SE | SW));

                    case _:
                        throw 'Invalid movement direction.';
                }
        }
    }

    public function getNextNodes(state: Int, callback: NextNodeCallback)
    {
        var x: Int = nodeToX(state);
        var y: Int = nodeToY(state);

        tryAddNeighbor(callback, x, y, N);
        tryAddNeighbor(callback, x, y, W);
        tryAddNeighbor(callback, x, y, S);
        tryAddNeighbor(callback, x, y, E);

        if (movementDirection.isDiagonal())
        {
            tryAddNeighbor(callback, x, y, NW);
            tryAddNeighbor(callback, x, y, NE);
            tryAddNeighbor(callback, x, y, SW);
            tryAddNeighbor(callback, x, y, SE);
        }
    }

    public function getHeuristicCost(from: Int, to: Int): Float
    {
        var fromX: Int = nodeToX(from);
        var fromY: Int = nodeToY(from);
        var toX: Int = nodeToX(to);
        var toY: Int = nodeToY(to);

        return heuristicFunction.getEstimate(fromX, fromY, toX, toY);
    }

    /**
     * Find the shortest path between two points on the 2D map.
     *
     * @param startX the x-coordinate of the starting point
     * @param startY the y-coordinate of the starting point
     * @param goalX the x-coordinate of the final point
     * @param goalY the y-coordinate of the final point
     * @param maxCost the maximum cost of the path; this optional parameter can be used to limit the search
     *                to a nearby area around the starting point, thus saving computational time since
     *                the algorithm will exit early if this range is exceeded
     * @return the result
     */
    public function solve(startX: Int, startY: Int, goalX: Int, goalY: Int, ?maxCost: Float): SearchResult2D
    {
        var start: Int = xyToNode(startX, startY);
        var goal: Int = xyToNode(goalX, goalY);
        var res: SearchResult = astar.solve(start, goal, maxCost);

        return {
            result: res.result,
            cost: res.cost,
            path: res.path == null ? null : res.path.map(s ->
            {
                x: nodeToX(s),
                y: nodeToY(s)
            })
        };
    }

    function tryAddNeighbor(callback: NextNodeCallback, x: Int, y: Int, direction: Direction)
    {
        var neighborX: Int = x + direction.dirX();
        var neighborY: Int = y + direction.dirY();

        if (!isInBounds(neighborX, neighborY))
        {
            // Out of bounds.
            return;
        }

        if (!canMoveFromCellInDirection(x, y, direction))
        {
            // Node not passable in the specified direction.
            // (i.e has no cost specified, or is blocked by a corner)
            return;
        }

        var neighbor: Int = xyToNode(neighborX, neighborY);
        var cost: Float = getCostInto(neighborX, neighborY, direction);

        callback(neighbor, cost);
    }

    function getCostInto(x: Int, y: Int, direction: Direction): Float
    {
        var cell: Int = getCellValue(x, y);
        if (costs.exists(cell) && costs[ cell ].exists(direction))
        {
            return costs[ cell ][ direction ];
        }
        else
        {
            throw 'getCost() called on point without cost: ${x} ${y} ${direction}';
        }
    }

    inline function getCellValue(x: Int, y: Int): Int
    {
        return map[ y * width + x ];
    }

    inline function isInBounds(x: Int, y: Int): Bool
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    inline function isCellPassable(x: Int, y: Int): Bool
    {
        return isInBounds(x, y) && costs.exists(getCellValue(x, y));
    }

    function getMinCost(directions: Int): Float
    {
        var found: Bool = false;
        var min: Float = 0;

        for (_ => dirCosts in costs)
        {
            for (dir => cost in dirCosts)
            {
                if (dir & directions > 0)
                {
                    if (!found || cost < min)
                    {
                        found = true;
                        min = cost;
                    }
                }
            }
        }

        return min;
    }

    function canMoveFromCellInDirection(x: Int, y: Int, direction: Direction): Bool
    {
        var neighborX: Int = x + direction.dirX();
        var neighborY: Int = y + direction.dirY();

        if (!isCellPassable(neighborX, neighborY))
        {
            // Neighbor in this direction has no costs specified.
            return false;
        }

        if (!costs[ getCellValue(neighborX, neighborY) ].exists(direction))
        {
            // Neighbor not passable from this direction.
            return false;
        }

        // Neighbor cell passable, check if diagonal movement is allowed.
        return
            switch movementDirection
            {
                // Given direction is not diagonal, no need to check.
                case _ if (!direction.isDiagonal()):
                    true;

                // Normal EightWay can move despite being obstructed by adjacents.
                case EightWay:
                    true;

                // One adjacent along the diagonal should be free in order to move diagonally.
                case EightWayHalfObstructed: isCellPassable(neighborX, y) || isCellPassable(x, neighborY);

                // Both adjacent along the diagonal should be free in order to move diagonally.
                case EightWayObstructed: isCellPassable(neighborX, y) && isCellPassable(x, neighborY);

                // Four-way movement, the first case should have handled this.
                default:
                    throw 'invalid movement direction';
            }
    }

    inline function xyToNode(x: Int, y: Int): Int
    {
        return (y << 16) | x;
    }

    inline function nodeToX(state: Int): Int
    {
        return state & 0xFFFF;
    }

    inline function nodeToY(state: Int): Int
    {
        return state >> 16;
    }

    /**
     * The default set of 8-directional costs.
     *
     * ```haxe
     * [
     *     Direction.N => 1.0,
     *     Direction.S => 1.0,
     *     Direction.E => 1.0,
     *     Direction.W => 1.0,
     *
     *     Direction.NW => sqrt(2),
     *     Direction.SW => sqrt(2),
     *     Direction.NE => sqrt(2),
     *     Direction.SE => sqrt(2),
     * ]
     * ```
     */
    public static var defaultCosts(get, never): Map<Direction, Float>;

    @:noCompletion static final sqrt2: Float = Math.sqrt(2);

    static inline function get_defaultCosts(): Map<Direction, Float>
    {
        return [
            Direction.N => 1.0,
            Direction.S => 1.0,
            Direction.E => 1.0,
            Direction.W => 1.0,

            Direction.NW => sqrt2,
            Direction.SW => sqrt2,
            Direction.NE => sqrt2,
            Direction.SE => sqrt2,
        ];
    }
}
