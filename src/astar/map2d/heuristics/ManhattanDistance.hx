package astar.map2d.heuristics;

class ManhattanDistance implements HeuristicFunction
{
    public inline function new()
    {
    }

    public function getEstimate(fromX: Int, fromY: Int, toX: Int, toY: Int): Float
    {
        var dx: Int = toX > fromX ? toX - fromX : fromX - toX;
        var dy: Int = toY > fromY ? toY - fromY : fromY - toY;

        return (dx + dy);
    }
}
