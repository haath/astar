package astar.map2d.heuristics;

interface HeuristicFunction
{
    function getEstimate(fromX: Int, fromY: Int, toX: Int, toY: Int): Float;
}
