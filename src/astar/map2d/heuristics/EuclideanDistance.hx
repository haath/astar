package astar.map2d.heuristics;

import Math.sqrt;


class EuclideanDistance implements HeuristicFunction
{
    public inline function new()
    {
    }

    public function getEstimate(fromX: Int, fromY: Int, toX: Int, toY: Int): Float
    {
        var dx: Int = toX - fromX;
        var dy: Int = toY - fromY;

        return sqrt((dx * dx) + (dy * dy));
    }
}
