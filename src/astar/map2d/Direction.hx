package astar.map2d;

enum abstract Direction(Int) to Int
{
    /** North **/
    var N = 1 << 0;

    /** West **/
    var W = 1 << 1;

    /** East **/
    var E = 1 << 2;

    /** South **/
    var S = 1 << 3;

    /** Northwest **/
    var NW = 1 << 4;

    /** Northeast **/
    var NE = 1 << 5;

    /** Southwest **/
    var SW = 1 << 6;

    /** Southeast**/
    var SE = 1 << 7;

    public inline function dirX(): Int
    {
        return
            switch this
            {
                case E | NE | SE:
                    1;

                case W | NW | SW:
                    -1;

                case _:
                    0;
            }
    }

    public inline function dirY(): Int
    {
        return
            switch this
            {
                case N | NE | NW:
                    1;

                case S | SE | SW:
                    -1;

                case _:
                    0;
            }
    }

    public inline function isDiagonal(): Bool
    {
        return this & (NW | NE | SW | SE) > 0;
    }

    @:allow(astar.SearchResult)
    public static inline function ofStep(fromX: Int, fromY: Int, toX: Int, toY: Int): Direction
    {
        return
            switch [ toX - fromX, toY - fromY ]
            {
                case [ 0, 1 ]:
                    N;
                case [ -1, 1 ]:
                    NW;
                case [ 1, 1 ]:
                    NE;

                case [ -1, 0 ]:
                    W;
                case [ 1, 0 ]:
                    E;

                case [ 0, -1 ]:
                    S;
                case [ -1, -1 ]:
                    SW;
                case [ 1, -1 ]:
                    SE;

                default:
                    throw "Invalid step";
            };
    }
}
