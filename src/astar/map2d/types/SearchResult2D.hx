package astar.map2d.types;

import astar.types.Result;


typedef SearchResult2D =
{
    /**
     * Value indicating whether the search was successful.
     * It is recommended to check this field before using `path`.
    **/
    var result: Result;

    /**
     * The total exact (non-heuristic) cost of the path.
     */
    var cost: Float;

    /**
     * The solution's path, as an ordered list of states.
     *
     * Check the `result` before using.
     */
    var path: Array<Point2D>;
}
