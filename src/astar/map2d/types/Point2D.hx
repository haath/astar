package astar.map2d.types;

typedef Point2D =
{
    var x: Int;

    var y: Int;
}
