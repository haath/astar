import astar.map2d.Map2D;


class Main
{
    static inline var SAMPLES = 10000;

    public static function main()
    {
        var cacheCost: Float = 0;
        var cacheBenefit: Float = 0;
        var solve: Float = 0;
        var solveToFillCache: Float = 0;
        var solveWithCache: Float = 0;

        for (i in 0...SAMPLES)
        {
            var result = run();

            cacheCost += result.cacheCost;
            cacheBenefit += result.cacheBenefit;
            solve += result.times.solveWithoutCache;
            solveToFillCache += result.times.solveToFillCache;
            solveWithCache += result.times.solveWithCache;
        }

        cacheCost = Math.round(cacheCost / SAMPLES);
        cacheBenefit = Math.round(cacheBenefit / SAMPLES);
        solve = Math.round((solve / SAMPLES) * 1000);
        solveToFillCache = Math.round((solveToFillCache / SAMPLES) * 1000);
        solveWithCache = Math.round((solveWithCache / SAMPLES) * 1000);

        Sys.println('Time to solve: ${solve}us');
        Sys.println('Time to solve & make cache: ${solveToFillCache}us');
        Sys.println('Time to solve cached: ${solveWithCache}us');
        Sys.println('Cost to construct cache: $cacheCost%');
        Sys.println('Benefit with cache: $cacheBenefit%');
    }

    static function run(): Dynamic
    {
        var graph: Map2D = new Map2D(10, 10, FourWay);

        // 0: floor
        // 1: wall
        // 2: Can only move north and south
        // @formatter:off
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 0, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];
        // @formatter:on
        var costs = [ 0 => Map2D.defaultCosts ];

        graph.setMap(world);
        graph.setCosts(costs);
        graph.solve(1, 0, 9, 9); // fill memory pools

        var timestamp: Timestamp = Timestamp.Now;

        graph.solve(1, 0, 9, 9);

        var solveWithoutCache: Float = timestamp.getElapsedMsReset();

        graph.configureCache(true, 128);

        timestamp.reset();

        graph.solve(1, 0, 9, 9);

        var solveToFillCache: Float = timestamp.getElapsedMsReset();

        graph.solve(0, 0, 9, 9);

        var solveWithCache: Float = timestamp.getElapsedMsReset();

        graph.solve(0, 0, 9, 9);

        var solveWithCache2: Float = timestamp.getElapsedMsReset();

        return {
            cacheCost: 100 * (solveToFillCache - solveWithoutCache) / solveWithoutCache,
            cacheBenefit: 100 * (solveWithoutCache - solveWithCache) / solveWithoutCache,
            times:
            {
                solveWithoutCache: solveWithoutCache,
                solveToFillCache: solveToFillCache,
                solveWithCache: solveWithCache
            }
        };
    }
}
