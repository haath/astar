class TestUtil
{
    public static function randInt(min: Int, max: Int): Int
    {
        return Math.floor(min + Math.random() * (max - min));
    }

    public static function pick<T>(arr: Array<T>): T
    {
        var i: Int = randInt(0, arr.length);
        return arr[ i ];
    }

    public static function shuffle<T>(arr: Array<T>): Array<T>
    {
        var order: Array<Float> = [ for (i in 0...arr.length) Math.random() ];
        var indices: Array<Int> = [ for (i in 0...arr.length) i ];
        indices.sort((a, b) -> order[ a ] < order[ b ] ? -1 : 1);

        var sorted: Array<T> = [ ];
        for (i in 0...arr.length)
        {
            sorted.push(arr[ indices[ i ] ]);
        }

        return sorted;
    }
}
