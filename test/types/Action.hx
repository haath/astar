package types;

enum abstract Action(Int) from Int to Int
{
    var None = 0;
    var Walk;
    var Drive;
    var RideTrain;
    var Fly;
    var Sail;
    var Swim;
    var DoNothing;
}
