package types;

import astar.Graph;
import astar.SearchResult;
import astar.types.NextNodeCallback;
import types.Action.*;
import types.State.*;
import utest.Assert;


class CustomGraph extends Graph
{
    public function new()
    {
        super();
    }

    public function getNextNodes(state: Int, callback: NextNodeCallback)
    {
        switch cast(state, State)
        {
            case Home:
                callback(Home, 1, DoNothing);
                callback(TrainStation, 5, Walk);
                callback(TrainStation, 3, Drive);
                callback(Port, 20, Walk);
                callback(Port, 10, Drive);
                callback(Airport, 30, Walk);
                callback(Airport, 20, Drive);

            case TrainStation:
                callback(Airport, 2, RideTrain);
                callback(Port, 3, RideTrain);
                callback(Home, 5, Walk);
                callback(Home, 3, Drive);

            case Port:
                callback(Island, 10, Sail);
                callback(Home, 20, Walk);
                callback(Home, 10, Drive);

            case Airport:
                callback(Island, 5, Fly);
                callback(Home, 30, Walk);
                callback(Home, 20, Drive);

            case Island:
                callback(IsWet, 1, Swim);

            case IsWet:
                Assert.fail('invalid state');

            default:
                Assert.fail('invalid state');
        }
    }

    public function getHeuristicCost(from: Int, to: Int): Float
    {
        return 0;
    }

    public function solve(from: Int, to: Int): SearchResult
    {
        return astar.solve(from, to);
    }
}
