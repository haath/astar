package types;

enum abstract State(Int) from Int to Int
{
    var Home;
    var TrainStation;
    var Port;
    var Airport;
    var Island;
    var IsWet;
}
