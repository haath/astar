package tests;

import Math.sqrt;
import astar.map2d.heuristics.DiagonalDistance;
import astar.map2d.heuristics.EuclideanDistance;
import astar.map2d.heuristics.HeuristicFunction;
import utest.Assert;
import utest.ITest;


class Test2DHeuristics implements ITest
{
    public function new()
    {
    }

    function testDiagonalDistance()
    {
        var heuristic: HeuristicFunction = new DiagonalDistance();

        Assert.floatEquals(1, heuristic.getEstimate(2, 3, 2, 2));
        Assert.floatEquals(0, heuristic.getEstimate(2, 3, 2, 3));
        Assert.floatEquals(1, heuristic.getEstimate(2, 4, 2, 3));
        Assert.floatEquals(sqrt(2), heuristic.getEstimate(3, 4, 2, 3), 1e-2);
        Assert.floatEquals(sqrt(2), heuristic.getEstimate(1, 4, 2, 3), 1e-2);
        Assert.floatEquals(sqrt(2), heuristic.getEstimate(1, 4, 2, 5), 1e-2);
        Assert.floatEquals(sqrt(8), heuristic.getEstimate(1, 1, 3, 3), 1e-2);
        Assert.floatEquals(5.82, heuristic.getEstimate(1, 1, 3, 6), 1e-2);
    }

    function testEuclideanDistance()
    {
        var heuristic: HeuristicFunction = new EuclideanDistance();

        Assert.floatEquals(1, heuristic.getEstimate(2, 3, 2, 2));
        Assert.floatEquals(0, heuristic.getEstimate(2, 3, 2, 3));
        Assert.floatEquals(1, heuristic.getEstimate(2, 4, 2, 3));
        Assert.floatEquals(sqrt(2), heuristic.getEstimate(3, 4, 2, 3));
        Assert.floatEquals(sqrt(2), heuristic.getEstimate(1, 4, 2, 3));
        Assert.floatEquals(sqrt(2), heuristic.getEstimate(1, 4, 2, 5));
    }
}
