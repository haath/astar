package tests;

import astar.Astar;
import astar.SearchResult;
import astar.cache.CacheResult;
import astar.cache.PathCache;
import astar.map2d.Map2D;
import astar.types.PathNode;
import astar.types.Result;
import utest.Assert;
import utest.ITest;


@:access(astar.Astar)
@:access(astar.Graph)
@:access(astar.map2d.Map2D)
@:access(astar.cache.PathCache)
@:access(astar.cache.BubbleList)
class TestPathCache implements ITest
{
    var cache: PathCache;

    public function new()
    {
    }

    function setup()
    {
        cache = new PathCache(1024);
    }

    function testHash()
    {
        // good enough is we get no collisions in a small grid.

        var size: Int = 100;

        var hashes: Map<Int, Bool> = [ ];

        for (x in 0...size)
        {
            for (y in 0...size)
            {
                var hash: Int = PathCache.hash(x, y);

                Assert.isFalse(hashes.exists(hash), 'hash collision $x $y -> $hash');

                hashes.set(hash, true);
            }
        }
    }

    function testAddSolution()
    {
        var graph: Map2D = loadTestGraph();
        var astar: Astar = graph.astar;

        var from: Int = graph.xyToNode(0, 0);
        var to: Int = graph.xyToNode(9, 9);

        var solution: SearchResult = astar.solve(from, to);
        Assert.equals(Result.Solved, solution.result);

        var endNode: PathNode = astar.findPath(from, to);
        Assert.notNull(endNode.previous);

        cache.addSolution(endNode);

        Assert.equals(solution.path.length - 1, countCachedPaths());

        for (startX in 0...10)
        {
            for (startY in 0...10)
            {
                var start: Int = graph.xyToNode(startX, startY);
                var goal: Int = graph.xyToNode(9, 9);
                var hash: Int = PathCache.hash(start, goal);

                var existsInPath: Bool = false;

                for (node in solution.path)
                {
                    if (node == start)
                    {
                        existsInPath = true;
                        break;
                    }
                }

                if (existsInPath && (startX != 9 || startY != 9))
                {
                    Assert.notNull(cache.get(hash), '($startX, $startY) $hash $existsInPath');
                }
                else
                {
                    Assert.isNull(cache.get(hash));
                }
            }
        }
    }

    function testSolveCached()
    {
        var graph: Map2D = loadTestGraph();
        var astar: Astar = graph.astar;

        var from: Int = graph.xyToNode(0, 0);
        var goal: Int = graph.xyToNode(9, 9);

        var solution: SearchResult = astar.solve(from, goal);
        var solutionPath: Array<Int> = solution.path.copy();
        var endNode: PathNode = astar.findPath(from, goal);

        cache.addSolution(endNode);

        for (startX in 0...10)
        {
            for (startY in 0...10)
            {
                var start: Int = graph.xyToNode(startX, startY);

                var existsInPath: Bool = false;

                for (node in solutionPath)
                {
                    if (node == start)
                    {
                        existsInPath = true;
                        break;
                    }
                }

                solution.path.splice(0, solution.path.length);
                @:privateAccess solution.cost = 0;

                var cacheResult: CacheResult = cache.solve(start, goal, solution, 0);

                if (existsInPath && (startX != 9 || startY != 9))
                {
                    Assert.equals(CacheResult.Hit, cacheResult);

                    Assert.equals(start, solution.path[ 0 ]);

                    Assert.equals(goal, solution.path[ solution.path.length - 1 ]);

                    Assert.equals(solution.path.length - 1, solution.cost);
                    Assert.equals(solution.path.length, solution.path.length);

                    for (i in 0...solution.path.length)
                    {
                        Assert.equals(solutionPath[ solutionPath.length - 1 - i ], solution.path[ solution.path.length - 1 - i ]);
                    }
                }
                else
                {
                    Assert.notEquals(CacheResult.Hit, cacheResult);
                }
            }
        }
    }

    function testReset()
    {
        var graph: Map2D = loadTestGraph();
        var astar: Astar = graph.astar;

        var from: Int = graph.xyToNode(0, 0);
        var to: Int = graph.xyToNode(9, 9);

        var endNode: PathNode = astar.findPath(from, to);

        cache.addSolution(endNode);

        Assert.equals(0, cache.cachedPathPool.getSize());
        Assert.equals(0, cache.cachedPathNodePool.getSize());

        cache.reset();

        Assert.equals(0, cache.cache.length);
        Assert.equals(0, countCachedPaths());
        Assert.isNull(cache.cache.head);
        Assert.isNull(cache.cache.middle);
        Assert.isNull(cache.cache.tail);

        Assert.equals(30, cache.cachedPathPool.getSize());
        Assert.equals(31, cache.cachedPathNodePool.getSize());

        // Test with freeMemory
        cache.addSolution(endNode);

        cache.reset(true);

        Assert.equals(0, cache.cache.length);
        Assert.equals(0, countCachedPaths());
        Assert.isNull(cache.cache.head);
        Assert.isNull(cache.cache.middle);
        Assert.isNull(cache.cache.tail);

        Assert.equals(0, cache.cachedPathPool.getSize());
        Assert.equals(0, cache.cachedPathNodePool.getSize());
    }

    function loadTestGraph(): Map2D
    {
        var graph: Map2D = new Map2D(10, 10, FourWay);

        // 0: floor
        // 1: wall
        // @formatter:off
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 0, 1, 1, 1, 1, // 4
            0, 0, 0, 0, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];
        // @formatter:on
        graph.setMap(world);
        graph.setCosts([ 0 => Map2D.defaultCosts ]);

        return graph;
    }

    function countCachedPaths(): Int
    {
        var count: Int = 0;

        for (path in cache.cacheHashMap)
        {
            if (path != null)
                count++;
        }

        return count;
    }
}
