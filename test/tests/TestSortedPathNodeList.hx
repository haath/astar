package tests;

import astar.types.PathNode;
import astar.types.PathNodePool;
import astar.types.SortedPathNodeList;
import utest.Assert;
import utest.ITest;


@:access(astar.types.PathNode)
@:access(astar.types.SortedPathNodeList)
class TestSortedPathNodeList implements ITest
{
    var list: SortedPathNodeList;

    public function new()
    {
    }

    function setupClass()
    {
    }

    function setup()
    {
        list = new SortedPathNodeList();
    }

    function testInsert()
    {
        var p0 = createNodeWithCost(0);
        var p5 = createNodeWithCost(5);
        var p10 = createNodeWithCost(10);

        var p8 = createNodeWithCost(8);
        var p3 = createNodeWithCost(3);

        list.insert(p5);
        Assert.equals(p5, list.min);
        Assert.equals(1, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.insert(p0);
        Assert.equals(p0, list.min);
        Assert.equals(2, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.insert(p10);
        Assert.equals(3, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.insert(p8);
        Assert.equals(4, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.insert(p3);
        Assert.equals(5, list.getSize());
        Assert.equals(list.size, list.getSize());

        Assert.equals(p0, list.min);

        Assert.equals(null, p0.left);
        Assert.equals(p3, p0.right);
        Assert.equals(p0, p5.left);
        Assert.equals(p10, p5.right);
        Assert.equals(p8, p10.left);
        Assert.equals(null, p10.right);
        Assert.equals(null, p8.left);
        Assert.equals(null, p8.right);
        Assert.equals(null, p3.left);
        Assert.equals(null, p3.right);

        Assert.floatEquals(0, list.popNext().totalCost);
        Assert.equals(4, list.getSize());
        Assert.floatEquals(3, list.popNext().totalCost);
        Assert.equals(3, list.getSize());
        Assert.floatEquals(5, list.popNext().totalCost);
        Assert.equals(2, list.getSize());
        Assert.floatEquals(8, list.popNext().totalCost);
        Assert.equals(1, list.getSize());
        Assert.floatEquals(10, list.popNext().totalCost);
        Assert.equals(0, list.getSize());
        Assert.isNull(list.popNext());
    }

    function testLargeRandom()
    {
        var nodePool: PathNodePool = new PathNodePool();
        var amount: Int = 200;

        // Do 2 tests with 1000 elements each.
        for (_ in 0...2)
        {
            var numbers: Array<Float> = [ for (i in 0...amount) i ];
            numbers = numbers.shuffle();

            for (i in 0...numbers.length)
            {
                var node: PathNode = nodePool.getNode(0, 0, numbers[ i ], 0);

                list.insert(node);

                var size: Int = list.getSize();
                Assert.equals(i + 1, size);
                Assert.equals(size, list.size);
                Assert.equals(list.size, list.getSize());
            }

            var expected: Float = 0;

            for (i in 0...amount)
            {
                var node: PathNode = list.popNext();
                Assert.floatEquals(expected, node.totalCost);

                nodePool.put(node);

                var size: Int = list.getSize();
                Assert.equals(amount - i - 1, size);
                Assert.equals(size, list.size);
                Assert.equals(list.size, list.getSize());

                expected++;
            }

            Assert.isNull(list.root);
            Assert.isNull(list.min);
            Assert.isNull(list.popNext());
            Assert.isTrue(list.isEmpty());
        }
    }

    function testClearIntoPool()
    {
        var pool: PathNodePool = new PathNodePool();

        for (i in 0...100)
        {
            list.insert(pool.getNode(0, 0, TestUtil.randInt(0, 100), 0));
        }

        Assert.equals(100, list.getSize());

        list.clear(pool);

        Assert.equals(0, list.getSize());
        Assert.equals(100, pool.getSize());
    }

    function testReplaceNode()
    {
        var pool: PathNodePool = new PathNodePool();

        var src: PathNode = pool.getNode(0, 0, 1, 0);
        var child1: PathNode = pool.getNode(0, 0, 1, 0);
        var child2: PathNode = pool.getNode(0, 0, 1, 0);
        src.left = child1;
        src.right = child2;
        child1.parent = src;
        child2.parent = src;

        list.root = src;

        var dst: PathNode = pool.getNode(0, 0, 1, 0);

        list.replaceNode(src, dst);

        Assert.isNull(src.left);
        Assert.isNull(src.right);
        Assert.isNull(src.parent);
    }

    function testRemove()
    {
        var n5 = insertCost(5);
        var n3 = insertCost(3);
        var n7 = insertCost(7);
        var n2 = insertCost(2);
        var n4 = insertCost(4);
        var n6 = insertCost(6);
        var n8 = insertCost(8);

        list.remove(n3);
        Assert.equals(6, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.remove(n5);
        Assert.equals(5, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.remove(n2);
        Assert.equals(4, list.getSize());
        Assert.equals(list.size, list.getSize());

        Assert.raises(() -> list.remove(n2), String);

        list.remove(n7);
        Assert.equals(3, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.remove(n8);
        Assert.equals(2, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.remove(n6);
        Assert.equals(1, list.getSize());
        Assert.equals(list.size, list.getSize());

        list.remove(n4);
        Assert.equals(0, list.getSize());
        Assert.equals(list.size, list.getSize());

        Assert.isNull(list.root);
        Assert.isNull(list.min);

        Assert.raises(() -> list.remove(n5), String);
    }

    function testLargeRandomWithPool()
    {
        var pool: PathNodePool = new PathNodePool();

        var numbers: Array<Int> = [ for (i in 0...10000) i ];
        numbers = numbers.shuffle();

        var inserted: Array<PathNode> = [ ];
        var currentMinimum = () ->
        {
            var min: Int = numbers.length;
            for (ins in inserted)
            {
                if (ins.totalCost < min)
                    min = Std.int(ins.totalCost);
            }
            return min;
        };

        for (i in 0...numbers.length)
        {
            var visBefore: String = list.visualize();
            var toRemove: PathNode = null;

            var node: PathNode = pool.getNode(0, 0, numbers[ i ], 0);

            list.insert(node);
            inserted.push(node);

            Assert.equals(inserted.length, list.getSize());
            Assert.equals(inserted.length, list.size);

            var visAfterAdd: String = list.visualize();

            var rand: Float = Math.random();

            // with a 5% chance, clear the list.
            if (rand < 0.05)
            {
                list.clear(pool);
                inserted.resize(0);
            }

            // With a 20% chance, remove an element.
            else if (rand < 0.25)
            {
                var idx: Int = TestUtil.randInt(0, inserted.length - 1);
                toRemove = inserted[ idx ];
                inserted.remove(toRemove);
                list.remove(toRemove);

                if (list.size > 0)
                {
                    var min: Int = currentMinimum();

                    Assert.equals(min, list.min.totalCost);
                }
            }

            // With a 20% chance, pop an element.
            else if (rand < 0.45)
            {
                var min: Int = currentMinimum();
                toRemove = list.popNext();

                inserted.remove(toRemove);

                Assert.equals(min, Std.int(toRemove.totalCost));
            }

            var validation: String = list.validateTree();
            if (list.getSize() != inserted.length || validation != "")
            {
                var visNow: String = list.visualize();

                throw '
Add: ${node.totalCost}
Remove: ${toRemove != null ? toRemove.totalCost : -1}
Validation: $validation
${list.getSize()} != ${inserted.length}
=== BEFORE ===
$visBefore
=== AFTER ADD ===
$visAfterAdd
=== AFTER REMOVE ===
$visNow
                ';
            }

            Assert.equals(inserted.length, list.getSize());
            Assert.equals(list.size, list.getSize());
        }
    }

    function insertCost(cost: Int): PathNode
    {
        var n = createNodeWithCost(cost);
        list.insert(n);
        return n;
    }

    function createNodeWithCost(cost: Int): PathNode
    {
        var node: PathNode = new PathNode();
        node.costFromStart = cost;
        node.estimatedCostToGoal = 0;
        return node;
    }
}
