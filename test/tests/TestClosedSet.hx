package tests;

import astar.types.PathNodePool;
import astar.wrappers.ClosedSet;
import utest.Assert;
import utest.ITest;


class TestClosedSet implements ITest
{
    var set: ClosedSet;

    public function new()
    {
    }

    function setup()
    {
        set = new ClosedSet();
    }

    function test()
    {
        Assert.isFalse(set.contains(12));

        var n = new PathNodePool().getNode(12, 0, 0, 0);

        set.addNode(n);

        Assert.isTrue(set.contains(12));
    }
}
