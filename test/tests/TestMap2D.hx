package tests;

import astar.Astar;
import astar.cache.PathCache;
import astar.map2d.Direction;
import astar.map2d.Map2D;
import astar.map2d.heuristics.EuclideanDistance;
import astar.map2d.types.Point2D;
import astar.map2d.types.SearchResult2D;
import astar.types.Result;
import utest.Assert;
import utest.ITest;


@:access(astar.Astar)
@:access(astar.Graph)
class TestMap2D implements ITest
{
    var graph: Map2D;

    public function new()
    {
    }

    function setup()
    {
        // Simple 10x10 grid
        graph = new Map2D(10, 10, FourWay);
        graph.setCosts(defaultCosts());
    }

    function testGetMinCost()
    {
        graph.setCosts([ 0 => [ N => 3, S => 2, W => 1, E => 4 ] ]);

        Assert.floatEquals(1, graph.getMinCost(0xFFFF));
    }

    function testGetCellCost()
    {
        // @formatter:off
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 1, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 1, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];
        // @formatter:on
        graph.setMap(world);
        graph.setCosts([ 0 => [ N => 3, S => 2, W => 1 ] ]);

        Assert.floatEquals(3, graph.getCostInto(0, 0, N));
        Assert.raises(() -> graph.getCostInto(0, 0, E));
        Assert.floatEquals(2, graph.getCostInto(0, 2, S));
    }

    function testSetInvalidWorld()
    {
        Assert.raises(() -> graph.setMap([ 1, 2, 3 ]), String);
    }

    function testStateHashing()
    {
        var state: Int = graph.xyToNode(12, 5);
        var x: Int = graph.nodeToX(state);
        var y: Int = graph.nodeToY(state);

        Assert.equals(12, x);
        Assert.equals(5, y);
    }

    function testCanMoveFromCellInDirection()
    {
        // 0: floor
        // 1: wall
        // 2: Can only move north and south
        // @formatter:off
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 1, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 1, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];
        // @formatter:on
        var costs = defaultCosts();
        costs.set(2, [ N => 1, S => 1 ]);

        graph.setMap(world);
        graph.setCosts(costs);

        graph.movementDirection = FourWay;
        Assert.isTrue(graph.canMoveFromCellInDirection(5, 3, E));
        Assert.isFalse(graph.canMoveFromCellInDirection(5, 3, N));
        Assert.isFalse(graph.canMoveFromCellInDirection(5, 3, W));
        Assert.isTrue(graph.canMoveFromCellInDirection(5, 3, S));

        graph.movementDirection = EightWay;
        Assert.isFalse(graph.canMoveFromCellInDirection(5, 3, NE));
        Assert.isTrue(graph.canMoveFromCellInDirection(5, 3, NW));

        graph.movementDirection = EightWayHalfObstructed;
        Assert.isFalse(graph.canMoveFromCellInDirection(5, 3, NE));
        Assert.isFalse(graph.canMoveFromCellInDirection(5, 3, NW));
        Assert.isTrue(graph.canMoveFromCellInDirection(5, 3, SE));
        Assert.isTrue(graph.canMoveFromCellInDirection(4, 4, NE));

        graph.movementDirection = EightWayObstructed;
        Assert.isFalse(graph.canMoveFromCellInDirection(5, 3, NE));
        Assert.isFalse(graph.canMoveFromCellInDirection(5, 3, NW));
        Assert.isFalse(graph.canMoveFromCellInDirection(5, 3, SW));
    }

    function testMapInvalidSize()
    {
        Assert.raises(() -> new Map2D(-5, 100, FourWay));
        Assert.raises(() -> new Map2D(100, -8, FourWay));
        Assert.raises(() -> new Map2D(95535, 100, FourWay));
        Assert.raises(() -> new Map2D(256, 95535, FourWay));
    }

    function testInvalidMovementDirectionInHeuristic()
    {
        graph.heuristicFunction = null;
        graph.movementDirection = cast -1;

        Assert.raises(() -> graph.setCosts(defaultCosts()));
    }

    function testSimplePaths()
    {
        doPathingTest();
    }

    function testSimplePathsWithCache()
    {
        graph.configureCache(true, 128);

        doPathingTest();

        Assert.equals(3, graph.astar.pathCache.hits);
        Assert.isTrue(graph.astar.pathCache.misses > 0);
    }

    function doPathingTest()
    {
        // 0: floor
        // 1: wall
        // 2: Can only move north and south
        // @formatter:off
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 0, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];
        // @formatter:on
        var costs = defaultCosts();
        costs.set(2, [ N => 1, S => 1 ]);

        graph.setMap(world);
        graph.setCosts(costs);

        // ================= STRAIGHT LINE =================
        var straightLine: SearchResult2D = graph.solve(1, 2, 8, 2);
        Assert.equals(Result.Solved, straightLine.result);
        Assert.equals(8, straightLine.path.length);
        Assert.equals(7, straightLine.cost);
        printRoute(straightLine.path);


        // ================= STRAIGHT LINE 2 ===============
        var straightLine2: SearchResult2D = graph.solve(2, 2, 8, 2);
        Assert.equals(Result.Solved, straightLine2.result);
        Assert.equals(7, straightLine2.path.length);
        Assert.equals(6, straightLine2.cost);
        printRoute(straightLine2.path);


        // ================= OVER WALL =====================
        var overWall: SearchResult2D = graph.solve(1, 9, 3, 9);
        Assert.equals(Result.Solved, overWall.result);
        Assert.equals(9, overWall.path.length);
        Assert.equals(8, overWall.cost);
        printRoute(overWall.path);


        // ================= COMPLICATED ===================
        var complicated: SearchResult2D = graph.solve(0, 0, 9, 9);
        Assert.equals(Result.Solved, complicated.result);
        Assert.equals(31, complicated.path.length);
        Assert.equals(30, complicated.cost);
        printRoute(complicated.path);


        // ================= MOVE THROUGH 2 =================
        var moveThrough2: SearchResult2D = graph.solve(3, 4, 3, 6);
        Assert.equals(Result.Solved, moveThrough2.result);
        Assert.equals(3, moveThrough2.path.length);
        Assert.equals(2, moveThrough2.cost);
        printRoute(moveThrough2.path);


        // ================= AVOID 2 =================
        var avoid2: SearchResult2D = graph.solve(2, 5, 4, 5);
        Assert.equals(Result.Solved, avoid2.result);
        Assert.equals(5, avoid2.path.length);
        Assert.equals(4, avoid2.cost);
        printRoute(avoid2.path);


        // ============= AVOID 2 REVERSE ============
        var avoid2rev: SearchResult2D = graph.solve(4, 5, 2, 5);
        Assert.equals(Result.Solved, avoid2rev.result);
        Assert.equals(5, avoid2rev.path.length);
        Assert.equals(4, avoid2rev.cost);
        printRoute(avoid2rev.path);


        // ================= BLOCKED =======================
        var blocked: SearchResult2D = graph.solve(5, 5, 9, 0);
        Assert.equals(Result.NoSolution, blocked.result);


        // ================ BLOCKED 2 ======================
        // to cover cache hit on NoSolution
        var blocked2: SearchResult2D = graph.solve(5, 5, 9, 0);
        Assert.equals(Result.NoSolution, blocked2.result);


        // =============== START END SAME ==================
        var startEndSame: SearchResult2D = graph.solve(5, 5, 5, 5);
        Assert.equals(Result.StartEndSame, startEndSame.result);


        // ============= STRAIGHT LINE EXCEED COST ===========
        var straightLine3: SearchResult2D = graph.solve(2, 2, 8, 2, 4.0);
        Assert.equals(Result.MaxCostExceeded, straightLine3.result);
        Assert.isNull(straightLine3.path);
        Assert.equals(0, straightLine3.cost);


        // ============= STRAIGHT LINE EXCEED COST WITHOUT CACHE ===========
        graph.resetCache();
        var straightLine4: SearchResult2D = graph.solve(2, 2, 8, 2, 4.0);
        Assert.equals(Result.MaxCostExceeded, straightLine4.result);
        Assert.isNull(straightLine4.path);
        Assert.equals(0, straightLine4.cost);


        // =============== COMPLICATED 8-way ===============
        graph.resetCache();
        graph.movementDirection = EightWay;
        var complicated8: SearchResult2D = graph.solve(0, 0, 9, 9);
        Assert.equals(Result.Solved, complicated8.result);
        Assert.equals(24, complicated8.path.length);
        Assert.floatEquals(25.89, complicated8.cost, 1e-2);
        printRoute(complicated8.path);
    }

    function testSolveCached()
    {
        // 0: floor
        // 1: wall
        // 2: Can only move north and south
        // @formatter:off
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 0, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];
        // @formatter:on
        var costs = defaultCosts();
        costs.set(2, [ N => 1, S => 1 ]);

        graph.setMap(world);
        graph.setCosts(costs);

        graph.configureCache(true, 512);
        graph.movementDirection = EightWay;
        graph.resetCache();


        // =============== COMPLICATED 8-way ===============
        var complicated8: SearchResult2D = graph.solve(0, 0, 9, 9);
        Assert.equals(Result.Solved, complicated8.result);
        Assert.equals(24, complicated8.path.length);
        Assert.floatEquals(25.89, complicated8.cost, 1e-2);

        Assert.equals(0, graph.astar.pathCache.hits);

        var complicated8cached: SearchResult2D = graph.solve(0, 0, 9, 9);

        Assert.equals(1, graph.astar.pathCache.hits);

        Assert.equals(complicated8.path.length, complicated8cached.path.length);
        Assert.equals(complicated8.path.length, complicated8cached.path.length);
        Assert.floatEquals(complicated8.cost, complicated8cached.cost);

        for (i in 0...complicated8.path.length)
        {
            Assert.equals(complicated8.path[ i ].x, complicated8cached.path[ i ].x);
            Assert.equals(complicated8.path[ i ].y, complicated8cached.path[ i ].y);
        }
    }

    function testIssueNo3()
    {
        graph = new Map2D(32, 32, EightWayObstructed);
        graph.setCosts(defaultCosts());
        graph.setHeuristic(new EuclideanDistance());

        var world: Array<Int> = [ for (_ in 0...32 * 32) 0 ];
        graph.setMap(world);

        var set = (x: Int) ->
        {
            world[ x ] = 1;
            var res = graph.solve(16, 16, 0, 0);
            Assert.equals(Result.Solved, res.result);
        };
        set(263);
        set(231);
        set(199);
        set(167);
        set(135);
        set(103);
        set(71);
        set(39);
        set(40);
        set(41);
        set(42);
        set(43);
        set(44);
        set(45);
        set(46);
        set(47);
        set(48);
        set(49);
        set(50);
        set(51);
        set(52);
        set(53);
        set(54);
        set(55);
        set(56);
        set(57);
        set(58);
        set(295);
        set(296);
        set(298);
        set(299);
        set(297);
    }

    function testConfigureCache()
    {
        var astar: Astar = graph.astar;

        Assert.isNull(astar.pathCache);
        Assert.isFalse(astar.cacheEnabled);

        Assert.raises(() -> astar.configureCache(true), String);

        astar.configureCache(true, 512);

        Assert.notNull(astar.pathCache);
        Assert.isTrue(astar.cacheEnabled);
        Assert.equals(512, astar.pathCache.size);

        var curCache: PathCache = astar.pathCache;

        astar.configureCache(false);

        Assert.notNull(astar.pathCache);
        Assert.isFalse(astar.cacheEnabled);
        Assert.equals(curCache, astar.pathCache);

        astar.configureCache(true);

        Assert.notNull(astar.pathCache);
        Assert.isTrue(astar.cacheEnabled);
        Assert.equals(curCache, astar.pathCache);

        astar.configureCache(false);

        Assert.notNull(astar.pathCache);
        Assert.isFalse(astar.cacheEnabled);
        Assert.equals(curCache, astar.pathCache);

        astar.configureCache(true, 256);

        Assert.notNull(astar.pathCache);
        Assert.isTrue(astar.cacheEnabled);
        Assert.equals(curCache, astar.pathCache);
        Assert.equals(256, astar.pathCache.size);
    }

    function testInvalidMovementDirection()
    {
        graph = new Map2D(10, 10, cast -1);

        // @formatter:off
        var world: Array<Int> = [
       // x 0  1  2  3  4  5  6  7  8  9     y
            0, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, // 1
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
            0, 0, 0, 0, 1, 0, 0, 0, 0, 0, // 3
            0, 0, 0, 0, 0, 1, 1, 1, 1, 1, // 4
            0, 0, 0, 2, 0, 0, 1, 0, 0, 0, // 5
            0, 0, 0, 0, 0, 0, 1, 0, 1, 0, // 6
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 7
            0, 0, 1, 0, 0, 0, 1, 0, 1, 0, // 8
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, // 9
        ];
        // @formatter:on
        // set a valid heuristic
        graph.setHeuristic(new EuclideanDistance());

        graph.setMap(world);
        graph.setCosts(defaultCosts());

        Assert.raises(() -> graph.canMoveFromCellInDirection(6, 0, NE));
    }

    static function defaultCosts(): Map<Int, Map<Direction, Float>>
    {
        return [ 0 => Map2D.defaultCosts ];
    }

    function printRoute(path: Array<Point2D>)
    {
        var buf = new StringBuf();
        buf.add('\n');

        var inPath = (x: Int, y: Int) ->
        {
            for (p in path)
            {
                if (p.x == x && p.y == y)
                    return true;
            }

            return false;
        };

        for (y in 0...graph.height)
        {
            for (x in 0...graph.width)
            {
                var val = graph.getCellValue(x, y);

                if (inPath(x, y))
                {
                    buf.add('x ');
                }
                else
                {
                    buf.add('$val ');
                }
            }

            buf.add('\n');
        }
    }
}
