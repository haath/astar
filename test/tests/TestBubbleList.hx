package tests;

import astar.cache.BubbleList;
import haxe.PosInfos;
import utest.Assert;
import utest.ITest;


class Item
{
    public var next: Item;
    public var previous: Item;
    public var value: Int;

    public inline function new(value: Int)
    {
        this.value = value;
    }
}

@:access(astar.cache.BubbleList)
class TestBubbleList implements ITest
{
    var list: BubbleList<Item>;

    public function new()
    {
    }

    function setup()
    {
        list = new BubbleList<Item>(5);
    }

    function testInsert()
    {
        insert(1);
        assertList([ 1 ], 1);

        insert(3);
        assertList([ 3, 1 ], 3);

        insert(2);
        assertList([ 3, 2, 1 ], 2);

        insert(4);
        assertList([ 3, 4, 2, 1 ], 4);

        insert(5);
        assertList([ 3, 4, 5, 2, 1 ], 5);

        // Now to exceed capacity.
        var ret: Item;
        ret = list.insert(new Item(9));
        Assert.equals(1, ret.value);
        assertList([ 3, 4, 9, 5, 2 ]);

        ret = list.insert(new Item(9));
        Assert.equals(2, ret.value);
        assertList([ 3, 4, 9, 9, 5 ]);

        ret = list.insert(new Item(9));
        Assert.equals(5, ret.value);
        assertList([ 3, 4, 9, 9, 9 ]);
    }

    function testBubble()
    {
        var i1 = insert(1);
        var i3 = insert(3);
        var i2 = insert(2);
        var i4 = insert(4);
        var i5 = insert(5);

        assertList([ 3, 4, 5, 2, 1 ], 5);

        list.bubble(i3);
        assertList([ 3, 4, 5, 2, 1 ], 5);

        list.bubble(i4);
        assertList([ 4, 3, 5, 2, 1 ], 5);

        list.bubble(i5);
        assertList([ 4, 5, 3, 2, 1 ], 3);

        list.bubble(i2);
        assertList([ 4, 5, 2, 3, 1 ], 2);

        list.bubble(i1);
        assertList([ 4, 5, 2, 1, 3 ], 2);

        list.bubble(i1);
        assertList([ 4, 5, 1, 2, 3 ], 1);

        list.bubble(i1);
        assertList([ 4, 1, 5, 2, 3 ], 5);

        list.bubble(i1);
        assertList([ 1, 4, 5, 2, 3 ], 5);
    }

    function testPop()
    {
        var i1 = insert(1);
        var i3 = insert(3);
        var i2 = insert(2);
        var i4 = insert(4);
        var i5 = insert(5);

        assertList([ 3, 4, 5, 2, 1 ], 5);

        Assert.equals(i1, list.pop());
        assertList([ 3, 4, 5, 2 ], 4);

        Assert.equals(i2, list.pop());
        assertList([ 3, 4, 5 ], 4);

        list.insert(i1);
        assertList([ 3, 1, 4, 5 ], 1);

        Assert.equals(i5, list.pop());
        assertList([ 3, 1, 4 ], 1);

        Assert.equals(i4, list.pop());
        assertList([ 3, 1 ], 3);

        Assert.equals(i1, list.pop());
        assertList([ 3 ], 3);

        Assert.equals(i3, list.pop());
        assertList([ ]);

        Assert.isNull(list.pop());

        Assert.isNull(list.head);
        Assert.isNull(list.tail);
        Assert.isNull(list.middle);
    }

    function testInsertBetween()
    {
        var i1 = new Item(1);
        var i2 = new Item(2);
        var i3 = new Item(3);

        list.insertBetween(null, null, i1);
        Assert.isNull(i1.next);
        Assert.isNull(i1.previous);
    }

    function testRemove()
    {
        list = new BubbleList<Item>(6);

        var i1 = insert(1);
        var i3 = insert(3);
        var i2 = insert(2);
        var i4 = insert(4);
        var i5 = insert(5);
        var i6 = insert(6);

        assertList([ 3, 4, 6, 5, 2, 1 ], 6);

        list.remove(i5);

        assertList([ 3, 4, 6, 2, 1 ], 2);

        list.remove(i6);

        assertList([ 3, 4, 2, 1 ], 4);

        list.remove(i3);

        assertList([ 4, 2, 1 ], 2);

        list.remove(i1);

        assertList([ 4, 2 ], 4);

        list.insert(i1);

        assertList([ 4, 1, 2 ], 1);

        list.remove(i2);

        assertList([ 4, 1 ], 4);

        list.remove(i4);

        assertList([ 1 ], 1);

        list.remove(i1);

        assertList([ ]);

        Assert.raises(() -> list.remove(i1), String);
    }

    function testLargeRandom()
    {
        final listSize: Int = 256;
        final iterations: Int = 1000;

        list = new BubbleList<Item>(listSize);

        var numbers: Array<Int> = [ for (i in 0...iterations) i ];
        numbers = numbers.shuffle();

        var expected: Array<Item> = [ ];

        for (i in 0...iterations)
        {
            var expectPop: Bool = list.length == listSize;

            var item: Item = new Item(numbers[ i ]);
            var popped: Item = list.insert(item);
            expected.push(item);

            if (popped != null)
            {
                var removed: Bool = expected.remove(popped);
                Assert.isTrue(removed, 'error removing popped item from expectation list: ${popped.value}');
            }
            else if (expectPop)
            {
                Assert.fail('listed exceeded capacity but did not pop during insert');
                return;
            }

            var sizeCorrect: Bool = true;
            if (i < listSize)
            {
                sizeCorrect = sizeCorrect && Assert.equals(i + 1, list.length);
                sizeCorrect = sizeCorrect && Assert.equals(i + 1, expected.length);
            }
            else
            {
                sizeCorrect = sizeCorrect && Assert.equals(listSize, list.length);
                sizeCorrect = sizeCorrect && Assert.equals(listSize, expected.length);
            }
            if (!sizeCorrect)
            {
                return;
            }

            // With a 30% chance, bubble an element.
            if (Math.random() < 0.3)
            {
                var idx: Int = TestUtil.randInt(0, expected.length);
                var toBubble: Item = expected[ idx ];
                if (toBubble == null)
                {
                    Assert.fail('test error: null item popped; list length: ${expected.length}');
                    return;
                }

                var lengthBefore: Int = list.length;
                list.bubble(toBubble);
                if (!Assert.equals(lengthBefore, list.length, 'bubbling changed list size from $lengthBefore to ${list.length}'))
                {
                    return;
                }

                if (list.length > 1)
                {
                    Assert.isFalse(list.head == list.tail, 'list with length ${list.length} collapsed after bubble');
                }
            }

            assertListOutOfOrder(expected);
        }
    }

    function insert(val: Int): Item
    {
        var i = new Item(val);
        if (list.length < list.capacity)
        {
            var r: Item = list.insert(i);
            Assert.isNull(r);
        }
        else
        {
            var r: Item = list.insert(i);
            Assert.notNull(r);
        }
        Assert.isTrue(list.length <= list.capacity);
        return i;
    }

    function assertList(expected: Array<Int>, ?middle: Int, ?pos: PosInfos)
    {
        Assert.equals(expected.length, list.length, pos);

        if (list.length == 0)
        {
            Assert.isNull(list.head);
            Assert.isNull(list.tail);
        }
        else
        {
            Assert.isNull(list.head.previous, 'head.previous not null', pos);
            Assert.isNull(list.tail.next, 'tail.next not null', pos);
        }

        if (middle != null)
        {
            Assert.equals(middle, list.middle.value);
        }

        var values: Array<Int> = [ ];
        var iter: Item = list.head;
        var equal: Bool = true;

        for (i in 0...expected.length)
        {
            Assert.notNull(iter);

            equal = equal && (expected[ i ] == iter.value);

            values.push(iter.value);

            if (iter == iter.next)
            {
                Assert.fail("Infinite loop.");
                return;
            }

            iter = iter.next;
        }

        Assert.equals(values.length, list.length);

        if (!equal)
        {
            Assert.fail('expected $expected but it is $values');
        }
        else
        {
            Assert.pass();
        }
    }

    function assertListOutOfOrder(expected: Array<Item>, ?pos: PosInfos)
    {
        var iter: Item = list.head;
        var expCopy: Array<Item> = expected.copy();

        if (!Assert.equals(expCopy.length, list.length, 'expected list to have ${expCopy.length} items but got ${list.length}', pos))
        {
            return;
        }

        while (iter != null)
        {
            var ret: Bool = expCopy.remove(iter);
            Assert.isTrue(ret, pos);

            if (!ret)
            {
                return;
            }

            iter = iter.next;
        }

        Assert.equals(0, expCopy.length, 'expected list to be empty but got ${expCopy.length} items', pos);
    }
}
