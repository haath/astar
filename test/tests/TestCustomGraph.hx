package tests;

import astar.SearchResult;
import astar.types.Result;
import haxe.PosInfos;
import types.Action.*;
import types.Action;
import types.CustomGraph;
import types.State.*;
import utest.Assert;
import utest.ITest;


class TestCustomGraph implements ITest
{
    var graph: CustomGraph;

    public function new()
    {
    }

    function setup()
    {
        graph = new CustomGraph();
    }

    function testGoSwimming()
    {
        var result: SearchResult = graph.solve(Home, IsWet);

        Assert.equals(Result.Solved, result.result);
        Assert.equals(11, result.cost);
        assertArray([ Home, TrainStation, Airport, Island, IsWet ], result.path);
        assertArray([ Action.None, Drive, RideTrain, Fly, Swim ], result.transitions);
    }

    function assertArray(expected: Array<Int>, actual: Array<Int>, ?pos: PosInfos)
    {
        var equals: Bool = Assert.equals(expected.length, actual.length, pos);

        if (equals)
        {
            for (i in 0...expected.length)
            {
                equals = equals && (expected[ i ] == actual[ i ]);
            }
        }

        Assert.isTrue(equals, 'expect $expected but got $actual');
    }
}
