import mcover.coverage.MCoverage;
import utest.Runner;
import utest.ui.Report;
import utest.ui.common.HeaderDisplayMode;


class TestMain
{
    static function main()
    {
        var runner = new Runner();
        runner.onComplete.add(onComplete);

        runner.addCases("tests", false);

        Report.create(runner, SuccessResultsDisplayMode.NeverShowSuccessResults, HeaderDisplayMode.AlwaysShowHeader);

        runner.run();
    }

    static function onComplete(runner: Runner)
    {
        #if !js
        var covLogger = MCoverage.getLogger();
        covLogger.report();
        #end
    }
}
